namespace GraphQLTournament.Model
{
    public class Player
    {
        public string Name { get; set; }
        public Shoots Shoots { get; set; }
        public string Team { get; set; }
    }
}