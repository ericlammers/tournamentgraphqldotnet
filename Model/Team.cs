namespace GraphQLTournament.Model
{
    public class Team
    {
        public int Id { get; set; } 
        public string Name { get; set; }
        public int TotalPlayers { get; set; }
        public int Rank { get; set; }
        public Level Level { get; set; }
    }
}