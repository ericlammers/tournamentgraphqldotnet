using System.Collections.Generic;
using System.Linq;
using GraphQLTournament.Model;

namespace GraphQLTournament.Repository
{
    public class TeamRepository : ITeamRepository
    {
        private static readonly List<Team> teams = new List<Team>()
        {
            new Team
            {
                Id = 1,
                Name = "Toronto",
                Rank = 1,
                TotalPlayers = 22,
                Level = Level.PROFESSIONAL
            },
            new Team
            {
                Id = 2,
                Name = "Montreal",
                Rank = 5,
                TotalPlayers = 24,
                Level = Level.PROFESSIONAL
            },
            new Team
            {
                Id = 3,
                Name = "Vancouver",
                Rank = 3,
                TotalPlayers = 21,
                Level = Level.AMATUER
            },
            new Team
            {
                Id = 4,
                Name = "Edmonton",
                Rank = 2,
                TotalPlayers = 22,
                Level = Level.HOUSELEAGUE
            },
            new Team
            {
                Id = 5,
                Name = "Calgary",
                Rank = 4,
                TotalPlayers = 20,
                Level = Level.AMATUER
            }
        };
        
        public List<Team> GetAllTeams()
        {
            return teams;
        }
        
        public Team GetTeam(string name)
        {
            return teams.First(team => team.Name == name);
        }

        public Team UpdateTeam(Team updatedTeam, int teamId)
        {
            var team = teams.First(t => t.Id == teamId);

            team.Name = updatedTeam.Name;
            team.Rank = updatedTeam.Rank;
            team.TotalPlayers = updatedTeam.TotalPlayers;

            return team;
        }
    }
}