using System.Collections.Generic;
using GraphQLTournament.Model;

namespace GraphQLTournament.Repository
{
    public interface IPlayerRepository
    {
        public List<Player> GetPlayers(string teamName);

        public Player AddPlayer(Player player);

        public Player DeletePlayer(string name);
    }
}