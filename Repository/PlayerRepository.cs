using System.Collections.Generic;
using System.Linq;
using GraphQLTournament.Model;

namespace GraphQLTournament.Repository
{
    public class PlayerRepository : IPlayerRepository
    {
        private readonly List<Player> players = new List<Player>
        {
            new Player
            {
                Name = "Eric",
                Shoots = Shoots.RIGHT,
                Team = "Toronto"
            },
            new Player
            {
                Name = "Eric",
                Shoots = Shoots.RIGHT,
                Team = "Toronto"
            },
            new Player
            {
                Name = "Ryan",
                Shoots = Shoots.LEFT,
                Team = "Toronto"
            },
            new Player
            {
                Name = "Bob",
                Shoots = Shoots.RIGHT,
                Team = "Toronto"
            },
            new Player
            {
                Name = "Tom",
                Shoots = Shoots.RIGHT,
                Team = "Calgary"
            },
            new Player
            {
                Name = "Rob",
                Shoots = Shoots.LEFT,
                Team = "Edmonton"
            },
            new Player
            {
                Name = "Kate",
                Shoots = Shoots.LEFT,
                Team = "Montreal"
            },
            new Player
            {
                Name = "Mitchel",
                Shoots = Shoots.RIGHT,
                Team = "Montreal"
            }
        };

        public List<Player> GetPlayers(string teamName)
        {
            return players.FindAll(player => player.Team == teamName).ToList();
        }
        
        public Player AddPlayer(Player player)
        {
            players.Add(player);
            return player;
        }

        public Player DeletePlayer(string name)
        {
            var playerToRemove = players.First(p => p.Name == name);

            players.Remove(playerToRemove);

            return playerToRemove;
        }
    }
}