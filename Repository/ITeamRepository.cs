using System.Collections.Generic;
using System.Linq;
using GraphQLTournament.Model;

namespace GraphQLTournament.Repository
{
    public interface ITeamRepository
    {
        public List<Team> GetAllTeams();
        
        public Team GetTeam(string name);

        public Team UpdateTeam(Team updatedTeam, int teamId);
    }
}