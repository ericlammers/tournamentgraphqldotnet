using GraphQL;
using GraphQL.Types;

namespace GraphQLTournament.GraphQL
{
    public class TournamentSchema : Schema
    {
        public TournamentSchema(IDependencyResolver resolver) 
            : base(resolver)
        {
            Query = resolver.Resolve<TournamentQuery>();
            Mutation = resolver.Resolve<TournamentMutation>();
        }
    }
}