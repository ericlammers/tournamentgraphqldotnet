using GraphQL.Types;
using GraphQLTournament.GraphQL.Types;
using GraphQLTournament.Model;
using GraphQLTournament.Repository;

namespace GraphQLTournament.GraphQL
{
    public class TournamentMutation : ObjectGraphType
    {
        public TournamentMutation(IPlayerRepository playerRepository, ITeamRepository teamRepository)
        {
            Field<PlayerType>(
                "addPlayer",
                arguments: new QueryArguments(new QueryArgument<NonNullGraphType<PlayerInputType>>
                    {Name = "player"}),
                resolve: context =>
                {
                    var player = context.GetArgument<Player>("player");
                    
                    // Todo understand why TryAsyncResolve was used
                    return playerRepository.AddPlayer(player);
                });

            Field<TeamType>(
                "updateTeam",
                arguments: new QueryArguments(
                    new QueryArgument<NonNullGraphType<TeamInputType>> { Name = "team" }, 
                    new QueryArgument<NonNullGraphType<IntGraphType>> { Name = "teamId" }),
                resolve: context =>
                {
                    var team = context.GetArgument<Team>("team");
                    var teamId = context.GetArgument<int>("teamId");

                    return teamRepository.UpdateTeam(team, teamId);
                }
            );
            
            Field<PlayerType>(
                "deletePlayer",
                arguments: new QueryArguments(new QueryArgument<NonNullGraphType<StringGraphType>> { Name = "name" }),
                resolve: context =>
                {
                    var name = context.GetArgument<string>("name");

                    return playerRepository.DeletePlayer(name);
                }
            );
        }
    }
}