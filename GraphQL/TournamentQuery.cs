using GraphQL.Types;
using GraphQLTournament.GraphQL.Types;
using GraphQLTournament.Repository;

namespace GraphQLTournament.GraphQL
{
    public class TournamentQuery : ObjectGraphType
    {
        private readonly ITeamRepository teamRepository;

        public TournamentQuery(ITeamRepository teamRepository)
        {
            Field<ListGraphType<TeamType>>(
                "teams",
                resolve: context => teamRepository.GetAllTeams());

            Field<TeamType>(
                "team",
                arguments: new QueryArguments(new QueryArgument<NonNullGraphType<StringGraphType>> 
                    { Name = "name" }),
                resolve: context =>
                {
                    var name = context.GetArgument<string>("name");
                    return teamRepository.GetTeam(name);
                });

        }
    }
}