using GraphQL.Types;

namespace GraphQLTournament.GraphQL.Types
{
    public class TeamInputType : InputObjectGraphType
    {
        public TeamInputType()
        {
            Name = "teamInput";
            Field<NonNullGraphType<StringGraphType>>("name");
            Field<NonNullGraphType<IntGraphType>>("rank");
            Field<NonNullGraphType<IntGraphType>>("totalPlayers");
        }
    }
}