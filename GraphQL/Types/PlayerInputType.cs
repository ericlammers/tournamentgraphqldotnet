using GraphQL.Types;

namespace GraphQLTournament.GraphQL.Types
{
    public class PlayerInputType : InputObjectGraphType
    {
        public PlayerInputType()
        {
            Name = "playerInput";
            Field<NonNullGraphType<StringGraphType>>("name");
            Field<NonNullGraphType<StringGraphType>>("team");
        }
    }
}