using GraphQL.Types;
using GraphQLTournament.Model;
using GraphQLTournament.Repository;

namespace GraphQLTournament.GraphQL.Types
{
    public class TeamType : ObjectGraphType<Team>
    {
        public TeamType(IPlayerRepository playerRepository)
        {
            Field(team => team.Id).Description("Uniquely identifies the team.");
            Field(team => team.Name);
            Field(team => team.Rank);
            Field(team => team.TotalPlayers);
            Field<LevelType>("Level", "The level of the team.");

            Field<ListGraphType<PlayerType>>(
                "players", 
                resolve: context => playerRepository.GetPlayers(context.Source.Name));
        }
    }
}