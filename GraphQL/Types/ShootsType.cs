using GraphQL.Types;
using GraphQLTournament.Model;

namespace GraphQLTournament.GraphQL.Types
{
    public class ShootsType : EnumerationGraphType<Shoots>
    {
        public ShootsType()
        {
            Name = "Shoots";
            Description = "The way a player shoots";
        }
    }
}