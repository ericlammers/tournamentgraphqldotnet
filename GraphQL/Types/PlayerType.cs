using GraphQL.Types;
using GraphQLTournament.Model;

namespace GraphQLTournament.GraphQL.Types
{
    public class PlayerType : ObjectGraphType<Player>
    {
        public PlayerType()
        {
            Field(player => player.Name);
            Field<ShootsType>("Shoots", "The way a player shoots.");
        }
    }
}