using GraphQL.Types;
using GraphQLTournament.Model;

namespace GraphQLTournament.GraphQL.Types
{
    public class LevelType : EnumerationGraphType<Level>
    {
        public LevelType()
        {
            Name = "Level";
            Description = "The level of the league the team is a part of.";
        }
    }
}