## Sample Queries:

#### Get All Teams
```
{ teams {
  name
  players {
    name
  }
}}
```

#### Get Team By Name
```
{ team(name: "Toronto") {
  	name
  	rank
  	totalPlayers
  	level
	}
}
```

## Sample Mutations

#### Create Player
```
mutation ($player: playerInput!) {
  addPlayer(player: $player) { name }
}

{
  "player": {
    "name": "Kathy",
    "team": "Toronto"
  }
}
```

#### Update Team
```
mutation ($team: teamInput!) {
  updateTeam(team: $team, teamId: 1) { 
    name
    id
    rank
    totalPlayers
  }
}

{
  "team": {
    "name": "Toronto",
    "rank": 20,
    "totalPlayers": 40
	}
}
```

#### Delete Player
```
mutation ($name: String!) {
  deletePlayer(name: $name) {
    name
    shoots
  }
}

{
  "name": "Eric"
}
```